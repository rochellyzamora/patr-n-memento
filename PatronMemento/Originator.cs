﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMemento
{
    class Originator
    {
        //propiedades
        private string nombre { get; set; }
        private string apellido { get; set; }
        private string telefono { get; set; }
        private string direccion { get; set; }

        //metodos set
        // agregamos un metodo que nos va a guardar el nombre de la persona
        public void setNombre(string nombre)
        {
            Console.WriteLine("Nombre:" + nombre);
            this.nombre = nombre;
        }
        //metodo que nos va a guardar el apellido de la persona
        public void setApellido(string apellido)
        {
            Console.WriteLine("Apellido:" + apellido);
            this.apellido = apellido;
        }
        //metodo que nos va a guardar el telefono de la persona
        public void setTelefono(string telefono)
        {
            Console.WriteLine("Telefono: " + telefono);
            this.telefono = telefono;
           
        }
        //metodo que nos va a guardar la direccion de la persona
        public void setDireccion(string direccion)
        {
            Console.WriteLine("Dirección: " + direccion);
            this.direccion = direccion;
           
        }
        //metodos get
        //metodo donde nos mostrara los datos que hemos ingresado anteriormente
        public string getNombre() { return nombre; }
        public string getApellido() { return apellido; }
        public string getTelefono() { return telefono; }
        public string getDireccion() { return direccion; }

        //Crea el punto de guardado del Memento
        //Guarda los datos ingresados previamente y muestra un mensaje diciendo lo que se esta realizando
        public Memento SalvaMemento()
        {
            Console.WriteLine("\n Salvando estado --\n");
            return new Memento(nombre, apellido, telefono, direccion);
        }

        //Restaura el Memento
        //Muestra por pantalla los datos que fueron ingresados como datos de origen  
        public void RestauraMemento(Memento memento)
        {
            Console.WriteLine("\n Restaurando estado --\n");
            this.setNombre(memento.getNombre());
            this.setApellido(memento.getApellido());
            this.setTelefono(memento.getTelefono());
            this.setDireccion(memento.getDireccion());
        }
    }
}
