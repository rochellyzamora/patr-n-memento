﻿using System;

namespace PatronMemento
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se instancia la clase Originator y se crea un objeto "originador" y se introduce los datos de las propiedades
            Originator originador = new Originator();
            originador.setNombre("Sam");
            originador.setApellido("Smith");
            originador.setTelefono("0993768298");
            originador.setDireccion("Calle 112 Avenida 103");

            //Se instancia la clase Caretaker, se crea el objeto "cuidador"
            //Guarda el estado
            CareTaker cuidador = new CareTaker();
            cuidador.setMemento(originador.SalvaMemento());

            //cambiamos los datos de las propiedades
            originador.setNombre("Pol");
            originador.setApellido("Granch");
            originador.setTelefono("0983211634");
            originador.setDireccion("Calle 12 Avenida 15");

            //Restaura el estado guardado
            originador.RestauraMemento(cuidador.getMemento());

            Console.ReadKey();
        }
    }
}
