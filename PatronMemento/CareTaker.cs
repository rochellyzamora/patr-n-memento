﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMemento
{
    class CareTaker // es la que cuida el Memento
    {
        //propiedad
        private Memento memento;

        //metodo
        public void setMemento (Memento memento)
        {
            this.memento = memento;
        }
        public Memento getMemento()
        {
            return memento;
        }
    }
}
