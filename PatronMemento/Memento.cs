﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMemento
{
    class Memento
    {
        //propiedades, las mismas del objeto a guardar
        private string nombre { get; set; }
        private string apellido { get; set; }
        private string telefono { get; set; }
        private string direccion { get; set; }

        //constructor de la clase
        public Memento(string nombre, string apellido, string telefono, string direccion)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.telefono = telefono;
            this.direccion = direccion;
        }
        //metodos set y get del Memento para obtener el estado del originador
        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public void setApellido(string nombre)
        {
            this.apellido = apellido;
        }
        public void setTelefono(string telefono)
        {
            this.telefono = telefono;
        }
        public void setDireccion(string direccion)
        {
            this.direccion = direccion;
        }
        public string getNombre() { return nombre; }
        public string getApellido() { return apellido; }
        public string getTelefono() { return telefono; }
        public string getDireccion() { return direccion; }

    }
}
